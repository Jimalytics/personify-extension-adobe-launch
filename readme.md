# Personify Adobe Launch Extension
Below are end to end instructions for updating and deploying Launch Extension Changes 
---

## Requirements
1. Node.js installed globably on your local machine. 

## Development
### Launch Config changes
* Configuration changes are handeled in `src/view/configuration/**.html`

### Action changes
* Action configuration changes are handeled in `src/view/actions/**.html`
* Action logic changes are handeled in `src/lib/action/**.js`

### Data Element changes
* Configuration changes are handeled in `src/view/lib/dataElements/**.js`

### New Actions, Data Elements and other Features
From the root folder of your project, run the following command to run update wizard 
`npx @adobe/reactor-scaffold`

### Testing Changes
Launch uses Node.js to spin up a local sandbox env to run and test you custom Launch Extension
To start the sandbox run the following command from the root folder of your project: 
`npx @adobe/reactor-sandbox`
This will start up a localhost that mimics a production launch env with my persona installed

## Deployment
Perform the following steps to publish changes
1. Update version number in the `extension.json` file
2. Create a extension package (.zip file) by running `npx @adobe/reactor-packager`
3. Run the code with your corresponding Adobe I/O creds. This will upload the zip file to the server 
4. Once zip is in place, you will have to make the package for release by executing the following HTTP Patch request via a tool like Postman or curl:
```javascript
curl -X PATCH \
  https://reactor.adobe.io/extension_packages/[PACKAGE-ID] \
  -H 'accept: application/vnd.api+json;revision=1' \
  -H 'content-type: application/vnd.api+json' \
  -H 'authorization: Bearer [TOKEN]' \
  -H 'cache-control: no-cache' \
  -H 'x-api-key: Activation-DTM' \
  -d \
'
{
    "data": {
        "id": "[PACKAGE-ID]",
        "type": "extension_packages",
        "meta": {
            "action": "release_private"
        }
    }
}
'
```

### Adobe resources
* https://developer.adobelaunch.com/guides/extensions/development-resources/
* https://developer.adobelaunch.com/guides/extensions/getting-started/
* https://developer.adobelaunch.com/guides/extensions/views/