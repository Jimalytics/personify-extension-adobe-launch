'use strict';

var cookie = require('@adobe/reactor-cookie');
var extensionSettings = turbine.getExtensionSettings();

module.exports = function(actionSettings) {
  if(!actionSettings.actionConfigured || !extensionSettings.configured){
    console.warn('action not configured');
    return; 
  }
    
var emptyCookie = {};
    
var emptyPersona, emptyConditions;
for(var personaName in extensionSettings.personas){
    // Creating Empty Persona with all the conditions having 'false' as default value
    if(!emptyCookie[personaName]){
        emptyCookie[personaName] = {};
    }

    for(var conditionName in extensionSettings.personas[personaName].conditions){
      emptyCookie[personaName][conditionName] = false; 
    }
}
  var mpCookie = cookie.get('personaConditions') ? JSON.parse(cookie.get('personaConditions')) : emptyCookie;
  if(!mpCookie[actionSettings.selectedCondition.personaId]){
    mpCookie[actionSettings.selectedCondition.personaId] = {};
  }

  mpCookie[actionSettings.selectedCondition.personaId][actionSettings.selectedCondition.conditionId] = true;
  cookie.set('personaConditions', mpCookie);

  //check for all conditions then set persoana
  if(Object.values(mpCookie[actionSettings.selectedCondition.personaId]).indexOf(false) == -1){
    cookie.set('myPersoana', actionSettings.selectedCondition.personaId);
  }

};
 