'use strict';

var cookie = require('@adobe/reactor-cookie');
var extensionSettings = turbine.getExtensionSettings();

module.exports = function(settings) {
  // TODO Return the data element value.
  var personaCookie = cookie.get('myPersoana');
  if(personaCookie){
    return extensionSettings.personas[personaCookie].name;
  }else{
    return '';
  }
};
