module.exports = {
  "dataElements": {
    "MyPersona": {
      "modulePath": "my-persona/src/lib/dataElements/getpersona.js",
      "settings": {}
    }
  },
  "rules": [{
    "id": "RL1551817527144",
    "name": "Another click",
    "events": [{
      "modulePath": "sandbox/click.js",
      "settings": {}
    }],
    "actions": [{
      "modulePath": "my-persona/src/lib/actions/personaAction.js",
      "settings": {
        "actionConfigured": true,
        "selectedCondition": {
          "personaId": "ABCDEFGHIJKLMNOPQRSTUVWX@AdobeOrg1551806456227",
          "conditionId": "238357493153"
        }
      }
    }]
  }, {
    "id": "RL1551817562802",
    "name": "Third Page Top",
    "events": [{
      "modulePath": "sandbox/pageTop.js",
      "settings": {}
    }],
    "actions": [{
      "modulePath": "my-persona/src/lib/actions/personaAction.js",
      "settings": {
        "actionConfigured": true,
        "selectedCondition": {
          "personaId": "ABCDEFGHIJKLMNOPQRSTUVWX@AdobeOrg1551806456227",
          "conditionId": "352740792495"
        }
      }
    }]
  }, {
    "id": "RL1551817624884",
    "name": "Fourth",
    "events": [{
      "modulePath": "sandbox/pageTop.js",
      "settings": {}
    }],
    "actions": [{
      "modulePath": "my-persona/src/lib/actions/personaAction.js",
      "settings": {
        "actionConfigured": true,
        "selectedCondition": {
          "personaId": "ABCDEFGHIJKLMNOPQRSTUVWX@AdobeOrg1551806469474",
          "conditionId": "866020738885"
        }
      }
    }]
  }, {
    "id": "RL1551817646910",
    "name": "Fifth",
    "events": [{
      "modulePath": "sandbox/pageTop.js",
      "settings": {}
    }],
    "actions": [{
      "modulePath": "my-persona/src/lib/actions/personaAction.js",
      "settings": {
        "actionConfigured": true,
        "selectedCondition": {
          "personaId": "ABCDEFGHIJKLMNOPQRSTUVWX@AdobeOrg1551806469474",
          "conditionId": "773269120878"
        }
      }
    }]
  }],
  "extensions": {
    "my-persona": {
      "displayName": "My Persona",
      "settings": {
        "configured": true,
        "personas": {
          "ABCDEFGHIJKLMNOPQRSTUVWX@AdobeOrg1551806456227": {
            "id": "ABCDEFGHIJKLMNOPQRSTUVWX@AdobeOrg1551806456227",
            "name": "P1",
            "conditions": {
              "789744258567": {
                "id": 789744258567,
                "conditionName": "C1"
              },
              "238357493153": {
                "id": 238357493153,
                "conditionName": "C2"
              },
              "352740792495": {
                "id": 352740792495,
                "conditionName": "C3"
              }
            }
          },
          "ABCDEFGHIJKLMNOPQRSTUVWX@AdobeOrg1551806469474": {
            "id": "ABCDEFGHIJKLMNOPQRSTUVWX@AdobeOrg1551806469474",
            "name": "P2",
            "conditions": {
              "866020738885": {
                "id": 866020738885,
                "conditionName": "C1"
              },
              "773269120878": {
                "id": 773269120878,
                "conditionName": "C2"
              }
            }
          },
          "ABCDEFGHIJKLMNOPQRSTUVWX@AdobeOrg1551806479249": {
            "id": "ABCDEFGHIJKLMNOPQRSTUVWX@AdobeOrg1551806479249",
            "name": "P3",
            "conditions": {
              "813657876162": {
                "id": 813657876162,
                "conditionName": "C1"
              }
            }
          },
          "ABCDEFGHIJKLMNOPQRSTUVWX@AdobeOrg1551806486818": {
            "id": "ABCDEFGHIJKLMNOPQRSTUVWX@AdobeOrg1551806486818",
            "name": "P4",
            "conditions": {}
          }
        }
      }
    }
  },
  "property": {
    "settings": {}
  },
  "buildInfo": {
    "turbineVersion": "25.4.0",
    "turbineBuildDate": "2019-03-05T20:33:19.891Z",
    "buildDate": "2019-03-05T20:33:19.891Z",
    "environment": "development"
  }
}